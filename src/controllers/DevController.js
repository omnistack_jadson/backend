const axios = require("axios");
const Dev = require("../models/Dev");
const parseStringAsArray = require("../utils/parseStringAsArray");
const { findConnections, sendMessage } = require("../websocket");

// index, show, store, update, destroy
module.exports = {
  async index(req, res) {
    const devs = await Dev.find();

    return res.json(devs);
  },

  async store(req, res) {
    // console.log(req.body)
    const { github_username, techs, latitude, longitude } = req.body;

    let dev = await Dev.findOne({ github_username }); // busca username na base de dados

    if (!dev) {
      // se não existe, cria

      const resApi = await axios.get(
        `https://api.github.com/users/${github_username}`
      );

      const { name = login, avatar_url, bio } = resApi.data; // Api data
      const techsArray = parseStringAsArray(techs); // techs: String to Array

      const location = {
        type: "Point",
        coordinates: [longitude, latitude],
      };
      // DB create
      dev = await Dev.create({
        name,
        github_username,
        bio,
        avatar_url,
        techs: techsArray,
        location,
      });

      // filtrar as conexoes num raio de 10km e com tech do novo usuario
      const sendSocketMessage = findConnections(
        { latitude, longitude },
        techsArray
      );

      sendMessage(sendSocketMessage, "newDev", dev);
    }

    return res.json(dev);
  },

  // nome, avatar_url, bio
  async update(req, res) {
    const { github_username } = req.params;
    let dev = await Dev.findOne({ github_username });

    const {
      name = dev.name,
      avatar_url = dev.avatar_url,
      bio = dev.bio,
      latitude = dev.location.coordinates[1],
      longitude = dev.location.coordinates[0],
    } = req.body;

    const location = {
      type: "Point",
      coordinates: [longitude, latitude],
    };

    dev = await Dev.findOneAndUpdate(
      github_username,
      {
        $set: {
          name,
          avatar_url,
          bio,
          location,
        },
      },
      { new: true }
    );

    return res.json(dev);
  },

  async destroy(req, res) {
    const { github_username } = req.params;

    const dev = await Dev.findOneAndDelete({ github_username });

    return res.json(dev);
  },
};
