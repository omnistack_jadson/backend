const Dev = require("../models/Dev");
const parseStringAsArray = require("../utils/parseStringAsArray");

module.exports = {
  async index(req, res) {
    // Buscar todos em um raio de 10km
    // Filtrar por tecnologias

    const { latitude, longitude, techs } = req.query;
    techsArray = parseStringAsArray(techs);

    const devs = await Dev.find({
      techs: {
        $in: techsArray, // see Mongo operators
      },
      location: {
        $near: {
          // perto de ...
          $geometry: {
            // coordenadas centrais
            type: "Point",
            coordinates: [longitude, latitude],
          },
          $maxDistance: 10000, // 10km
        },
      },
    });
    return res.json({ devs });
  },
};
