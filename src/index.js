require("dotenv").config();

const express = require("express");
const http = require("http");
const cors = require("cors");
const mongoose = require("mongoose");

const routes = require("./routes");
const { setupWebsocket } = require("./websocket");

// criar um arquivo .env a a partir do .env.example
const DB_URL = process.env.DB_URL;

const app = express();
const server = http.createServer(app);

mongoose.connect(DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use(cors()); // ({ origin: <link>})
app.use(express.json()); // entender json
app.use(routes); // rotas

server.listen(3333, () => {
  console.log("Servidor online");
});
